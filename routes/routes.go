package routes

import (
	"github.com/labstack/echo/v4"
	home "gitlab.com/leiendrulat/blog/handler/home"
)

func Routes(e *echo.Echo) {
	e.GET("/home", home.Home)

}
